### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 63f9a7f2-257e-4f11-bf92-da6de3d9c34c
using Pkg

# ╔═╡ 06f0866b-6fa4-411a-bd9b-47687ebdac7b
using Markdown

# ╔═╡ 61da8950-0fd8-461c-a175-608ffa1cc024
using InteractiveUtils

# ╔═╡ bc36ca80-42c6-40ed-8df1-13a444d41fc0
using DataStructures

# ╔═╡ 19a6f94a-2e50-42dc-af53-b65fba55a726
using PlutoUI

# ╔═╡ cfc74ca4-acc4-4ace-905d-ba38c1ff4f5f
struct State
	name::String
	position::Int64
	number_of_dirt::Vector{Int}
end

# ╔═╡ 4d148bce-a756-4e1e-9f3d-89f1b25aa3d2
struct Action
	name::String
	cost::Int64
end

# ╔═╡ b2661035-3067-4da6-9b26-48725080a7a6
md"# AIG710S FIRST GROUP ASSIGNMENT
### PROBLEM 1
"

# ╔═╡ 5e405d48-3495-4c03-a9e3-1548f73e0a95
md"""
![A* Algorithm](https://i.ytimg.com/vi/amlkE0g-YFU/maxresdefault.jpg)
"""

# ╔═╡ 5360d506-f525-4e12-9516-86a183864bd6
md"### Adding Cost to Actions
"

# ╔═╡ e2aa54bc-c5ad-41ac-9b04-adedf699cf50
begin
	MOVE_WEST = Action("MOVE WEST", 3)
	MOVE_EAST = Action("MOVE EAST", 3)
	REMAIN    = Action("REMAIN", 1)
	COLLECT =   Action("COLLECT", 5)
end

# ╔═╡ f69f52fe-a7e4-4a84-bc7e-0e6a853603b3
md"### Defining States
"

# ╔═╡ 3ce61123-939c-4a25-931f-0f90b838a7af
begin
	STATE_1 = State("STATE 1", 1, [1, 3, 2, 1])
	STATE_2 = State("STATE 2", 2, [1, 3, 2, 1])
	STATE_3 = State("STATE 3", 3, [1, 3, 2, 1])
	STATE_4 = State("STATE 4", 4, [1, 3, 2, 1])
	STATE_5 = State("STATE 5", 1, [0, 3, 2, 1])
	STATE_6 = State("STATE 6", 2, [0, 3, 2, 1])
	STATE_7 = State("STATE 7", 3, [0, 3, 2, 1])
	STATE_8 = State("STATE 8", 4, [0, 3, 2, 1])
	STATE_9 = State("STATE 9", 4, [0, 3, 2, 0])
end

# ╔═╡ 8485489a-5510-4367-9071-2fd81325adb5
begin
	MODEL = Dict()
	push!(MODEL, STATE_1 => [(MOVE_EAST, STATE_2), (MOVE_WEST, STATE_1), (REMAIN, STATE_1), (COLLECT, STATE_5)])
	push!(MODEL, STATE_2 => [(MOVE_EAST, STATE_3), (MOVE_WEST, STATE_1), (REMAIN, STATE_2)])
	push!(MODEL, STATE_3 => [(MOVE_EAST, STATE_4), (MOVE_WEST, STATE_2), (REMAIN, STATE_3)])
	push!(MODEL, STATE_4 => [(MOVE_EAST, STATE_4), (MOVE_WEST, STATE_3), (REMAIN, STATE_4)])
	push!(MODEL, STATE_5 => [( MOVE_WEST, STATE_5), (MOVE_EAST, STATE_6), (REMAIN, STATE_5)])
	push!(MODEL, STATE_6 => [( MOVE_WEST, STATE_5), (MOVE_EAST, STATE_7), (REMAIN, STATE_6)])
	push!(MODEL, STATE_7 => [( MOVE_WEST, STATE_6), (MOVE_EAST, STATE_8), (REMAIN, STATE_7)])
	MODEL
end

# ╔═╡ 1ac11660-b97c-4a42-823d-0de029cc14c3
function return_Result(MODEL, ancestors, initial, goal)
	
	resultSet = []
	explorer = goal
		while !(explorer == initial)
				current_state_ancestor = ancestors[explorer]
				related_transitions = MODEL[current_state_ancestor]
			for single_transitions in related_transitions
				if single_transitions[2] == explorer
					push!(resultSet, single_transitions[1])
					break
				else
					continue
				end
			end
				explorer = current_state_ancestor
		end
	return resultSet
end

# ╔═╡ 1ccf214c-8d1a-429f-81f7-3894e0512a37
function getHeuristic(x)	
	total = 0
	for i in 1:length(x)
		total += x[i]
	end
  return total
end

# ╔═╡ bac6adca-61f0-4c9d-a741-a9c5e901b96d
function addQueue(queue::PriorityQueue{State, Int64}, state::State,
cost::Int64)
	enqueue!(queue, state, cost)
	return queue
end

# ╔═╡ db7cd1fb-3858-4a30-b318-151927361ebe
function removeQueue(queue::PriorityQueue{State, Int64})
	removed = dequeue!(queue)
	return (removed, queue)
end

# ╔═╡ f128984b-b3d3-4e70-85e5-41f4cdadedc2
function astar(initial, transition_dict, goal_state, all_candidates,
		add_candidate, remove_candidate)
		explored = []
		ancestors = Dict{State, State}()
	     total_cost = 0
		the_candidates = add_candidate(all_candidates, initial, 0)
		parent = initial
	
	
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			 current_state = t1
			 the_candidates = t2
				
			 push!(explored, current_state)
             candidates = transition_dict[current_state]
			 for single_candidate in candidates
			   println(the_candidates)
			
				  if !(single_candidate[2] in explored)
					   push!(ancestors, single_candidate[2] => current_state)
					
					
					if (single_candidate[2] in  goal_state)
							return return_Result(transition_dict, ancestors,
							  initial, single_candidate[2])
					  else
					 
							the_candidates = add_candidate(the_candidates,
							single_candidate[2], single_candidate[1].cost + getHeuristic(single_candidate[2].number_of_dirt))
					
					end
				  end
				
			  end
         end
     end
	println(the_candidates)
end

# ╔═╡ 8c63bb0b-4f5e-45e5-b286-37f1fc74f5af
md"#### Select Initial State :"

# ╔═╡ 055d62c2-b69f-4643-bbd0-db99d5f8e1bb
begin
	@bind initial Select(["STATE_1", "STATE_2", "STATE_3"])
end

# ╔═╡ 47308413-1ec6-418e-aadc-be7676ca3984
if String(initial) == "STATE_1"
	INITIAL_STATE = STATE_1
elseif String(initial) == "STATE_2"
	INITIAL_STATE = STATE_2
elseif String(initial) == "STATE_3"
	INITIAL_STATE = STATE_3
end

# ╔═╡ e7e83a20-9819-49db-912b-3024ac9b9208
md"#### Select Goal State :"

# ╔═╡ 5028801c-065a-4843-a9b8-a2c2c81aefb2
begin
	@bind goal Select(["STATE_4", "STATE_5", "STATE_6"])
end

# ╔═╡ 4b6c3581-4f3e-4295-8c4a-1a28318d8aa8
if String(goal) == "STATE_4"
	GOAL_STATE = STATE_4
elseif String(goal) == "STATE_5"
	GOAL_STATE = STATE_5
elseif String(goal) == "STATE_6"
	GOAL_STATE = STATE_6
end

# ╔═╡ 9048e246-67a4-406d-9197-a300c97cc3a6
begin
ALL_CANDIDATES = PriorityQueue{State,Int64}(Base.Order.Reverse)
result_actions = astar(INITIAL_STATE, MODEL,[GOAL_STATE],ALL_CANDIDATES, addQueue, removeQueue)
	with_terminal() do
		println(result_actions)
	end
end

# ╔═╡ Cell order:
# ╠═63f9a7f2-257e-4f11-bf92-da6de3d9c34c
# ╠═06f0866b-6fa4-411a-bd9b-47687ebdac7b
# ╠═61da8950-0fd8-461c-a175-608ffa1cc024
# ╠═bc36ca80-42c6-40ed-8df1-13a444d41fc0
# ╠═19a6f94a-2e50-42dc-af53-b65fba55a726
# ╠═cfc74ca4-acc4-4ace-905d-ba38c1ff4f5f
# ╠═4d148bce-a756-4e1e-9f3d-89f1b25aa3d2
# ╟─b2661035-3067-4da6-9b26-48725080a7a6
# ╟─5e405d48-3495-4c03-a9e3-1548f73e0a95
# ╟─5360d506-f525-4e12-9516-86a183864bd6
# ╠═e2aa54bc-c5ad-41ac-9b04-adedf699cf50
# ╟─f69f52fe-a7e4-4a84-bc7e-0e6a853603b3
# ╠═3ce61123-939c-4a25-931f-0f90b838a7af
# ╠═8485489a-5510-4367-9071-2fd81325adb5
# ╠═1ac11660-b97c-4a42-823d-0de029cc14c3
# ╠═1ccf214c-8d1a-429f-81f7-3894e0512a37
# ╠═bac6adca-61f0-4c9d-a741-a9c5e901b96d
# ╠═db7cd1fb-3858-4a30-b318-151927361ebe
# ╠═f128984b-b3d3-4e70-85e5-41f4cdadedc2
# ╠═8c63bb0b-4f5e-45e5-b286-37f1fc74f5af
# ╟─055d62c2-b69f-4643-bbd0-db99d5f8e1bb
# ╟─47308413-1ec6-418e-aadc-be7676ca3984
# ╟─e7e83a20-9819-49db-912b-3024ac9b9208
# ╟─5028801c-065a-4843-a9b8-a2c2c81aefb2
# ╟─4b6c3581-4f3e-4295-8c4a-1a28318d8aa8
# ╠═9048e246-67a4-406d-9197-a300c97cc3a6
