### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 9ee6623e-3f99-49d0-83e6-df9a57853195
import Pkg


# ╔═╡ ecc4d839-ec7b-44fa-98df-474471e7151b
Pkg.add("PlutoUI")

# ╔═╡ 00c2cb7e-ba4b-4e82-9bc7-98de8c7dcd91
using PlutoUI

# ╔═╡ 806412dc-cf8c-4655-8030-ba31023b4aae
using Markdown

# ╔═╡ ced93050-5ce8-4d2b-a5e5-7b4ec8f24b16
using InteractiveUtils

# ╔═╡ a6515e4b-4f94-42b9-93e9-e57704a7611b
md"# AIG710S FIRST GROUP ASSIGNMENT
### PROBLEM 2
"

# ╔═╡ 6842e0e7-265e-4063-baa4-9b0058291829

md"""
![setup of the tower of a hanoi](https://blog.testlodge.com/wp-content/uploads/2018/04/alpha-and-beta-testing.png)
"""

# ╔═╡ 7f1370fe-03d2-4e0a-bc7b-ce0761f1f6dc
md"
In adversarial search, a game is a problem with an initial state, multiple
players (usually two), a utility function, a goal test and the order of execution
between players (succession function). You are tasked to write a program in
Julia that:
- captures the complete description of a game provided by a user (at
runtime);
- finds a solution for the game using the α − β pruning.
"

# ╔═╡ 37b8e8b5-afd3-4398-a7a6-979798aac510
md"### Function Definition"

# ╔═╡ 539169cd-1ad1-49ad-bf78-d4f1950b7006
function pruning_process(max_branch, index,alpha,beta,condition,components)
    if max_branch == 1
        first_component = components[index]
        return first_component
    end
    if condition
		best = alpha
		for i in 1:2
			val = pruning_process(max_branch - 1,i,alpha,beta,false,components)
			best = best >= val ? best : val
			alpha = alpha >= val ? alpha : val
			if beta <= alpha
				break
			end
		end
		return best
    else
        best = beta
		for i in 1:2
			val = pruning_process(max_branch - 1,i,alpha,beta,true,components)
			best = best <= val ? best : val
			beta = beta <= val ? beta : val
			if beta <= alpha
				break
			end
		end
		return best
    end
end

# ╔═╡ d1f79ae6-0d32-4d26-8c03-0c2f848ed001
md"Enter the number of items:"

# ╔═╡ d394937c-9141-44a1-b6f4-f6cbd5a09fbe
@bind items_n TextField()

# ╔═╡ b1c82802-3254-450a-a034-d85304590e6c
begin
	items = 0
	if items_n != ""
		items = Int64(items_n==0 ? 0 : BigFloat(items_n))
	end
end

# ╔═╡ f74080f8-b7c1-4c44-8ec3-fde7d41b05c0
md"ENTER BETA VALUE:"

# ╔═╡ ed1e0439-bc86-46a9-9f99-6af019567f9a
@bind minimim_n TextField()

# ╔═╡ c736f42f-383e-48cb-a146-1359790942bd
begin
	minimim = 0
	if minimim_n != ""
		minimim = Int64(minimim_n==0 ? 0 : BigFloat(minimim_n))
	end
end

# ╔═╡ b0f80343-7fd2-491a-9e10-3d849c7c7276
md"ENTER ALPHA VALUE:"

# ╔═╡ 30f8b9a4-9fcd-4e2b-8f30-0b9dcf5f8e85
@bind maximum_n TextField()

# ╔═╡ 6ffc3a3c-1858-4499-9fcd-3a8138bb0f21
begin
	maximum = 0
	if maximum_n != ""
		maximum = Int64(maximum_n==0 ? 0 : BigFloat(maximum_n))
	end
end

# ╔═╡ fd1a7980-49c0-4055-b683-8da668d10d8c
md"ENTER MAX NO BRANKES:"

# ╔═╡ a64ded44-c96d-4722-a593-ec6d34e807d4
@bind max_branch_n TextField()

# ╔═╡ 5c423532-4a18-47cb-99f5-0d4db0175336
begin
	max_branch = 0
	if max_branch_n != ""
		max_branch = Int64(max_branch_n==0 ? 0 : BigFloat(max_branch_n))
	end
end

# ╔═╡ 695ec6cc-403b-4099-9108-df4b49122dd9
components = []

# ╔═╡ e05f3606-1e41-4097-824c-d746b69cd61b
for x in 1:10
    print("ENTER :")
    push!(components, 5)
end

# ╔═╡ 92571ee6-2ffb-466e-a39b-a15496022bc9
md"START GAME: (Y/N):"

# ╔═╡ a0a3473b-e1dd-40c8-8422-11828581829b
@bind option Select(["YES", "NO"])

# ╔═╡ 927533e3-0a38-4eeb-afca-cc4b33753f98
if String(option) == "YES"
    xx = pruning_process(max_branch,1,minimim,maximum,true,components)
    with_terminal() do
		println("---------------------------------")
		println("----------- SOLUTION ------------")
		println(xx)
		println("---------------------------------")
	end
else
    with_terminal() do
		println("---------------------------------")
		println("------- GAME CANCELLED ----------")
		println("---------------------------------")
	end
end

# ╔═╡ Cell order:
# ╟─9ee6623e-3f99-49d0-83e6-df9a57853195
# ╟─ecc4d839-ec7b-44fa-98df-474471e7151b
# ╟─00c2cb7e-ba4b-4e82-9bc7-98de8c7dcd91
# ╟─806412dc-cf8c-4655-8030-ba31023b4aae
# ╟─ced93050-5ce8-4d2b-a5e5-7b4ec8f24b16
# ╟─a6515e4b-4f94-42b9-93e9-e57704a7611b
# ╟─6842e0e7-265e-4063-baa4-9b0058291829
# ╟─7f1370fe-03d2-4e0a-bc7b-ce0761f1f6dc
# ╟─37b8e8b5-afd3-4398-a7a6-979798aac510
# ╠═539169cd-1ad1-49ad-bf78-d4f1950b7006
# ╟─d1f79ae6-0d32-4d26-8c03-0c2f848ed001
# ╠═d394937c-9141-44a1-b6f4-f6cbd5a09fbe
# ╟─b1c82802-3254-450a-a034-d85304590e6c
# ╟─f74080f8-b7c1-4c44-8ec3-fde7d41b05c0
# ╠═ed1e0439-bc86-46a9-9f99-6af019567f9a
# ╟─c736f42f-383e-48cb-a146-1359790942bd
# ╟─b0f80343-7fd2-491a-9e10-3d849c7c7276
# ╠═30f8b9a4-9fcd-4e2b-8f30-0b9dcf5f8e85
# ╟─6ffc3a3c-1858-4499-9fcd-3a8138bb0f21
# ╟─fd1a7980-49c0-4055-b683-8da668d10d8c
# ╠═a64ded44-c96d-4722-a593-ec6d34e807d4
# ╟─5c423532-4a18-47cb-99f5-0d4db0175336
# ╟─695ec6cc-403b-4099-9108-df4b49122dd9
# ╟─e05f3606-1e41-4097-824c-d746b69cd61b
# ╟─92571ee6-2ffb-466e-a39b-a15496022bc9
# ╟─a0a3473b-e1dd-40c8-8422-11828581829b
# ╠═927533e3-0a38-4eeb-afca-cc4b33753f98
